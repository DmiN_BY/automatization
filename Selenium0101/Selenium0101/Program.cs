﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;

namespace Selenium0101
{
    class Program
    {
        /*
        public void clickWhenReady(By locator, int timeout)
        {
            WebElement element = null;
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            element = wait.until(ExpectedConditions.elementToBeClickable(locator));
            element.click();
        }
        */
        static void Main(string[] args)
        {
            /*
            FirefoxOptions options = new FirefoxOptions();
            options.UseLegacyImplementation = true;
            IWebDriver browserInstance = new FirefoxDriver(options);
            */


            string pageTitle;
            string filmDescription;
            const string SearchResultsLinksXpathLocator = "//div[@class='info']/p[@class='name']/a";
            const string FilmDescriptionXpathLocator = "//div[@class='brand_words film-synopsys']";
            const string SearchFieldXpathLocator = "//*[@name='kp_query']";
            const string SearchButtonXpathLocator = "//*[@value='искать!']";
            const string WhatToFind = "осьминог";

            IWebDriver browserInstance = new ChromeDriver();
            WebDriverWait wait = new WebDriverWait(browserInstance, TimeSpan.FromSeconds(10));
            browserInstance.Url = "http://kinopoisk.ru";
            pageTitle = browserInstance.Title;
            Console.WriteLine(pageTitle);
            //browserInstance.Navigate().GoToUrl("http://www.seleniumhq.org");
            //browserInstance.Navigate().Back();
            IWebElement fSearch = browserInstance.FindElement(By.XPath(SearchFieldXpathLocator));
            fSearch.SendKeys(WhatToFind);
            IWebElement bSearch = browserInstance.FindElement(By.XPath(SearchButtonXpathLocator));
            bSearch.Click();
            IList<IWebElement> lnkResultList = browserInstance.FindElements(By.XPath(SearchResultsLinksXpathLocator));
            Console.WriteLine(lnkResultList.Count);
            for (int i=0; i< lnkResultList.Count; i++)
            {
                //Console.WriteLine(i);
                IList<IWebElement> lnkResultRefreshedList = browserInstance.FindElements(By.XPath(SearchResultsLinksXpathLocator));
                IWebElement clickableLink = wait.Until(ExpectedConditions.ElementToBeClickable(lnkResultRefreshedList[i]));
                Console.WriteLine(clickableLink.Text);
                clickableLink.Click();
                try
                {
                    IWebElement divFilmText = browserInstance.FindElement(By.XPath(FilmDescriptionXpathLocator));
                    filmDescription = divFilmText.Text;
                    
                }
                catch(Exception)
                {
                    filmDescription = "No any data about this film.";
                }
                Console.WriteLine(filmDescription);
                //lnkResultRefreshedList[i].Click();
                browserInstance.Navigate().Back();
            }
            Console.ReadKey();
            browserInstance.Close();
        }
    }
}

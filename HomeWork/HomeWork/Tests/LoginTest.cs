﻿using System;
using NUnit.Framework;
using HomeWork.Pages;

namespace HomeWork.Tests
{
    [TestFixture]
    public class LoginTest
    {
        private MainPage kinopoiskMainPage;
        private const string _USER_PROFILE_LINK_TEXT = "Мой КиноПоиск";
        private const string _EMAIL_VALID = "xeda@p33.org";
        private const string _PASSWORD_VALID = "kinovinodomino";

        [OneTimeTearDown]
        public void TearDownForEverySuite()
        {
            kinopoiskMainPage.ExitBrowser();
        }

        [SetUp]
        public void SetUpForEveryCase()
        {
            kinopoiskMainPage = new MainPage();
       } 

        [TearDown]
        public void TearDownForEveryCase()
        {
            kinopoiskMainPage.LogOut();
        }

        [Test]
        public void LoginWithValidCredentials()
        {
            //kinopoiskMainPage.LogIn(_EMAIL_VALID, _PASSWORD_VALID);
            kinopoiskMainPage.CallLoginForm();
            kinopoiskMainPage.FillEmail(_EMAIL_VALID);
            kinopoiskMainPage.FillPassword(_PASSWORD_VALID);
            kinopoiskMainPage.ClickOnLoginButton();
            StringAssert.AreEqualIgnoringCase(kinopoiskMainPage.GetUserProfileLinkText(), _USER_PROFILE_LINK_TEXT);
        }
    }
}

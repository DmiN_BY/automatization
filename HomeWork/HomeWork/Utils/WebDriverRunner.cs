﻿using System;
using System.Configuration;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;

namespace HomeWork.Utils
{
    public class WebDriverRunner
    {
        private static IWebDriver driver;

        public WebDriverRunner()
        {
            driver = null;
        }

        public static IWebDriver GetDriver(string browser)
        {
            browser = browser.ToLower();
            switch (browser)
            {
                case "firefox":
                    if (driver == null)
                    {
                        string firefoxBinaryPath = ConfigurationManager.AppSettings["firefox_binary_path"];
                        FirefoxOptions driverOptions = new FirefoxOptions();
                        driverOptions.UseLegacyImplementation = true;
                        driverOptions.BrowserExecutableLocation = firefoxBinaryPath;
                        driver = new FirefoxDriver(driverOptions);
                    }
                    break;
                case "chrome":
                    if (driver == null)
                    {
                        ChromeOptions driverOptions = new ChromeOptions();
                        driverOptions.AddArgument("start-maximized");
                        driver = new ChromeDriver(driverOptions);
                        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(50);
                    }
                    break;
                case "ie":
                    if (driver == null)
                    {
                        // call ie driver here
                    }
                    break;
            }
            return driver;
        }
    }
}

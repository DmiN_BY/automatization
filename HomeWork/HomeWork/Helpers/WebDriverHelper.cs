﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using HomeWork.Utils;
using NLog;

namespace HomeWork.Helpers
{
    public class WebDriverHelper
    {
        private static IWebDriver driver;
        private static WebDriverWait wait;
        private static Logger logger = LogManager.GetCurrentClassLogger();
        
        // Logger configured to output messages to file "log.txt"
        // and output messages to console.

        public WebDriverHelper()
        {
            string requestedBrowser = ConfigurationManager.AppSettings["browser"];
            logger.Info("Requested browser = " + requestedBrowser);
            driver = WebDriverRunner.GetDriver(requestedBrowser);
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            logger.Info("Driver instance created: " + requestedBrowser);
        }

        public static void SwitchDriverToFrame(string id)
        {
            driver.SwitchTo().Frame(id);
            logger.Info("Driver switched to frame id={0} .", id);
        }

        public static void OpenNewPage(string url)
        {
            driver.Url = url;
            logger.Info("Open page having url={0} .", url);
        }

        public static IWebElement WaitWhileClickable(string locator)
        {
            IWebElement element = wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(locator)));
            return element;
        }

        public static IWebElement WaitWhileClickable(IWebElement element)
        {
            element = wait.Until(ExpectedConditions.ElementToBeClickable(element));
            return element;
        }

        public static void ClickOnElement(string locator)
        {
            logger.Info("Click on element having locator={0} .", locator);
            WaitWhileClickable(locator).Click();
        }

        public static void SendTextToElement(string locator, string text)
        {
            logger.Info("Send text={0} to element having locator={1} .", text, locator);
            WaitWhileClickable(locator).SendKeys(text);
        }

        public static string GetElementText(string locator)
        {
            logger.Info("Get text from element having locator={0} .", locator);
            string text = WaitWhileClickable(locator).Text;
            logger.Info("From element having locator={0} got text={1}.", locator, text);
            return text;
        }

        public static void CloseBrowser()
        {
            driver.Close();
            logger.Info("Browser closed.");
        }
    }
}

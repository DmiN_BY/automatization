﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace HomeWork
{
    [TestFixture]
    public class HomeTask02test
    {
        const string StartPageUrl = "https://www.kinopoisk.ru/";
        const string SearchResultsLinksXpathLocator = "//div[@class='info']/p[@class='name']/a";
        const string FilmDescriptionXpathLocator = "//div[@class='brand_words film-synopsys']";
        const string SearchFieldXpathLocator = "//*[@name='kp_query']";
        const string SearchButtonXpathLocator = "//*[@value='искать!']";
        const string ShowAllXpathLocator = "//p[@class='show_all']/a";
        const string LoginLinkXpathLocator = "//a[text()='Войти на сайт']";
        const string LoginInputXpathLocator = "//input[@name='login']";
        const string LoginButtonXpathLocator = "//button[@type='submit']";
        const string ProfileLinkXpathLocator = "//a[text()='Мой КиноПоиск']";
        const string PasswordInputXpathLocator = "//input[@name='password']";
        const string LoginFrameId = "kp2-authapi-iframe";
        const string _WhatToFindFirstValue = "осьминог";
        const string _WhatToFindSecondValue = "кактус";
        const string RegisteredEmail = "xeda@p33.org";
        const string RegisteredPassword = "kinovinodomino";

        IWebElement clickableElement;
        IWebDriver browserInstance;
        WebDriverWait wait;

        [OneTimeSetUp]
        public void StartBrowser()
        {
            ChromeOptions instanceOptions = new ChromeOptions();
            instanceOptions.AddArgument("start-maximized");
            browserInstance = new ChromeDriver(instanceOptions);
        }

        [OneTimeTearDown]
        public void CloseBrowser()
        {
            browserInstance.Close();
        }

        [SetUp]
        public void SetupForEveryTestcase()
        {
            wait = new WebDriverWait(browserInstance, TimeSpan.FromSeconds(10));
            browserInstance.Url = StartPageUrl;
        }

        [Test]
        [Description("Check that login using valid credentials is successful.")]
        public void LoginSuccessful()
        {
            WaitWhileClickable(LoginLinkXpathLocator).Click();
            browserInstance.SwitchTo().Frame(LoginFrameId);
            WaitWhileClickable(LoginInputXpathLocator).SendKeys(RegisteredEmail);
            WaitWhileClickable(PasswordInputXpathLocator).SendKeys(RegisteredPassword);
            WaitWhileClickable(LoginButtonXpathLocator).Click();
            StringAssert.AreEqualIgnoringCase(WaitWhileClickable(ProfileLinkXpathLocator).Text, "Мой КиноПоиск");
        }

        [Test]
        [Description("Check that page is successfully open")]
        [Ignore("not used in this task")]
        public void PageSuccessfullyOpen()
        {
            Assert.AreEqual(browserInstance.Url, StartPageUrl);
        }

        [TestCase(_WhatToFindFirstValue)]
        [Description("Check that page is successfully open")]
        [Ignore("not used in this task")]
        public void OutputContainPageTitle(string filmToFind)
        {
            StringAssert.Contains(browserInstance.Title, HomeTask02(filmToFind));
        }

        [TestCase(_WhatToFindFirstValue)]
        [Description("Check that HomeTask02 method return not NULL value")]
        [Ignore("not used in this task")]
        public void HomeTask02ReturnNotNullValue(string filmToFind)
        {
            Assert.IsNotNull(HomeTask02(_WhatToFindFirstValue));
        }

        [TestCase(_WhatToFindFirstValue)]
        [Description("Check that film without description processed correctly")]
        [Ignore("not used in this task")]
        public void ExistFilmWithoutDescription(string filmToFind)
        {
            StringAssert.Contains("No any data about this film.", HomeTask02(_WhatToFindFirstValue));
        }

        [TestCase(_WhatToFindFirstValue)]
        [TestCase(_WhatToFindSecondValue)]
        [Description("Check that at least one film was successfully found")]
        [Ignore("not used in this task")]
        public void FoundAtLeastOneFilm(string filmToFind)
        {
            StringAssert.DoesNotContain("Films displayed on the screen: 0", HomeTask02(_WhatToFindFirstValue));
        }

        // method implemented HomeTask02
        public string HomeTask02(string whatToFind)
        {
            string textToOutput = "\nDZ3 Dmitrij Nepachalovich.\n";
            textToOutput += (browserInstance.Title + "\n");
            IWebElement fSearch = browserInstance.FindElement(By.XPath(SearchFieldXpathLocator));
            fSearch.SendKeys(whatToFind);
            IWebElement bSearch = browserInstance.FindElement(By.XPath(SearchButtonXpathLocator));
            bSearch.Click();
            clickableElement = wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(ShowAllXpathLocator)));
            clickableElement.Click();
            IList<IWebElement> lnkResultList = browserInstance.FindElements(By.XPath(SearchResultsLinksXpathLocator));
            textToOutput += ("\nFilms displayed on the screen: " + lnkResultList.Count + "\n");

            for (int i = 0; i < lnkResultList.Count; i++)
            {
                IList<IWebElement> lnkResultRefreshedList = browserInstance.FindElements(By.XPath(SearchResultsLinksXpathLocator));
                clickableElement = wait.Until(ExpectedConditions.ElementToBeClickable(lnkResultRefreshedList[i]));
                if (clickableElement.Text.ToUpper().Contains(whatToFind.ToUpper()))
                {
                    textToOutput += ("\nFilm: " + clickableElement.Text + "\n");
                    clickableElement.Click();
                    try
                    {
                        IWebElement divFilmText = browserInstance.FindElement(By.XPath(FilmDescriptionXpathLocator));
                        textToOutput += (divFilmText.Text + "\n");
                    }
                    catch (Exception)
                    {
                        textToOutput += "No any data about this film.\n";
                    }
                    browserInstance.Navigate().Back();
                }
            }
            
            // --- output results to console ---
            Console.WriteLine(textToOutput);
            return textToOutput;
            /*
            // --- output results to file ---
            using (StreamWriter outputFile = new StreamWriter(@"OutputFile.txt"))
                outputFile.WriteLine(textToOutput);
            */
        }

        private IWebElement WaitWhileClickable(string locator)
        {
            IWebElement element = wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(locator)));
            return element;
        }
    }
}

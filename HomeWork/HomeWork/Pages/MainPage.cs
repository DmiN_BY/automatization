﻿using HomeWork.Helpers;

namespace HomeWork.Pages
{
    class MainPage:BasePage
    {
        private const string _URL_START_PAGE = @"https://www.kinopoisk.ru/";
        private const string _LINK_SEARCH_RESULT_XPATH = "//div[@class='info']/p[@class='name']/a";
        private const string _DIV_FILM_DESCRIPTION_XPATH = "//div[@class='brand_words film-synopsys']";
        private const string _FIELD_SEARCH_XPATH = "//*[@name='kp_query']";
        private const string _BUTTON_SEARCH_XPATH = "//*[@value='искать!']";
        private const string _LINK_SHOW_ALL_XPATH = "//p[@class='show_all']/a";
        private const string _LINK_LOGIN_XPATH = "//a[text()='Войти на сайт']";
        private const string _INPUT_LOGON_XPATH = "//input[@name='login']";
        private const string _BUTTON_LOGIN_XPATH = "//button[@type='submit']";
        private const string _LINK_PROFILE_XPATH = "//a[text()='Мой КиноПоиск']";
        private const string _LINK_LOGOUT_XPATH = "//a[text()='выйти']";
        private const string _INPUT_PASSWORD_XPATH = "//input[@name='password']";
        private const string _FRAME_LOGIN_ID = "kp2-authapi-iframe";
        private const string _DIV_ERROR_LOGIN = "//div[text()='Вы ошиблись в почте или пароле']";
        //private const string _WhatToFindFirstValue = "осьминог";


        public MainPage()
        {
            WebDriverHelper.OpenNewPage(_URL_START_PAGE);
        }

        public void LogIn(string email, string password)
        {
            WebDriverHelper.ClickOnElement(_LINK_LOGIN_XPATH);
            WebDriverHelper.SwitchDriverToFrame(_FRAME_LOGIN_ID);
            WebDriverHelper.SendTextToElement(_INPUT_LOGON_XPATH, email);
            WebDriverHelper.SendTextToElement(_INPUT_PASSWORD_XPATH, password);
            WebDriverHelper.ClickOnElement(_BUTTON_LOGIN_XPATH);
        }

        public void FillEmail(string email)
        {
            WebDriverHelper.SendTextToElement(_INPUT_LOGON_XPATH, email);
        }

        public void FillPassword(string password)
        {
            WebDriverHelper.SendTextToElement(_INPUT_PASSWORD_XPATH, password);
        }

        public void CallLoginForm()
        {
            WebDriverHelper.ClickOnElement(_LINK_LOGIN_XPATH);
            WebDriverHelper.SwitchDriverToFrame(_FRAME_LOGIN_ID);
        }

        public void ClickOnLoginButton()
        {
            WebDriverHelper.ClickOnElement(_BUTTON_LOGIN_XPATH);
        }

        public string GetUserProfileLinkText()
        {
            return WebDriverHelper.GetElementText(_LINK_PROFILE_XPATH);
        }
        public void LogOut()
        {
            WebDriverHelper.ClickOnElement(_LINK_LOGOUT_XPATH);
        }
    }
}

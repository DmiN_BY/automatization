﻿using HomeWork.Helpers;

namespace HomeWork.Pages
{
    public class BasePage
    {
        WebDriverHelper p;
        public BasePage()
        {
            new WebDriverHelper();
        }
        
        public void ExitBrowser()
        {
            WebDriverHelper.CloseBrowser();
        }
    }
}

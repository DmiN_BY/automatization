﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace HomeTask03
{
    class PageHelper
    {
        private const string _PageUrl = "https://www.kinopoisk.ru/";
        public string PageUrl
        {
            get { return _PageUrl; }
        }

        const string SearchResultsLinksXpathLocator = "//div[@class='info']/p[@class='name']/a";
        const string FilmDescriptionXpathLocator = "//div[@class='brand_words film-synopsys']";
        const string SearchFieldXpathLocator = "//*[@name='kp_query']";
        const string SearchButtonXpathLocator = "//*[@value='искать!']";
        const string ShowAllXpathLocator = "//p[@class='show_all']/a";

        IWebDriver browser;

        public void StartBrowser()
        {
            browser = new ChromeDriver();
            browser.Url = _PageUrl;
            //return browser;
        }

        public string GetCurrentUrl()
        {
            string url = browser.Url;
            return url;
        }

        public void CloseBrowser()
        {
            browser.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using NUnit.Framework;
using System.IO;


namespace HomeTask03
{
    [TestFixture]
    public class UnitTest1
    {
        const string SearchResultsLinksXpathLocator = "//div[@class='info']/p[@class='name']/a";
        const string FilmDescriptionXpathLocator = "//div[@class='brand_words film-synopsys']";
        const string SearchFieldXpathLocator = "//*[@name='kp_query']";
        const string SearchButtonXpathLocator = "//*[@value='искать!']";
        const string ShowAllXpathLocator = "//p[@class='show_all']/a";
        const string WhatToFind = "осьминог";
        string textToOutput = "\nDZ3 Dmitrij Nepachalovich.\n";
        IWebDriver browserInstance;
        WebDriverWait wait;
        IWebElement clickableLink;
        PageHelper kinopoisk = new PageHelper();

        [OneTimeSetUp]
        public void SetupForTestsuite()
        {

            kinopoisk.StartBrowser();
            //wait = new WebDriverWait(browserInstance, TimeSpan.FromSeconds(10));
        }

        [Test, Description("Check that start page is successfully opens")]
        public void PageSuccessfullyOpen()
        {
            string expectedUrl = kinopoisk.PageUrl;
            string actualUrl = kinopoisk.GetCurrentUrl();
            Assert.AreEqual(actualUrl, expectedUrl);
        }

        [OneTimeTearDown]
        public void TeardownForTestsuite()
        {
            kinopoisk.CloseBrowser();
        }
    }
}

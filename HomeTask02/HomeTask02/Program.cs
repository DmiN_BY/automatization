﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using System.IO;

namespace HomeTask02
{
    class Program
    {
        static void Main(string[] args)
        {
            const string SearchResultsLinksXpathLocator = "//div[@class='info']/p[@class='name']/a";
            const string FilmDescriptionXpathLocator = "//div[@class='brand_words film-synopsys']";
            const string SearchFieldXpathLocator = "//*[@name='kp_query']";
            const string SearchButtonXpathLocator = "//*[@value='искать!']";
            const string ShowAllXpathLocator = "//p[@class='show_all']/a";
            const string WhatToFind = "осьминог";
            string textToOutput = "\nDZ2 Dmitrij Nepachalovich.\n";
            IWebElement clickableLink;

            // ----- setup -----
            IWebDriver browserInstance = new ChromeDriver();
            WebDriverWait wait = new WebDriverWait(browserInstance, TimeSpan.FromSeconds(10));
            browserInstance.Url = "http://kinopoisk.ru";

            // ----- execution ------
            textToOutput += (browserInstance.Title + "\n");
            IWebElement fSearch = browserInstance.FindElement(By.XPath(SearchFieldXpathLocator));
            fSearch.SendKeys(WhatToFind);
            IWebElement bSearch = browserInstance.FindElement(By.XPath(SearchButtonXpathLocator));
            bSearch.Click();
            clickableLink = wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(ShowAllXpathLocator)));
            clickableLink.Click();
            IList<IWebElement> lnkResultList = browserInstance.FindElements(By.XPath(SearchResultsLinksXpathLocator));
            textToOutput += ("\nFilms displayed on the screen: " + lnkResultList.Count + "\n");
            
            for (int i = 0; i < lnkResultList.Count; i++)
            {
                IList<IWebElement> lnkResultRefreshedList = browserInstance.FindElements(By.XPath(SearchResultsLinksXpathLocator));
                clickableLink = wait.Until(ExpectedConditions.ElementToBeClickable(lnkResultRefreshedList[i]));
                if (clickableLink.Text.ToUpper().Contains(WhatToFind.ToUpper()))
                {
                    textToOutput += ("\nFilm: " + clickableLink.Text + "\n");
                    clickableLink.Click();
                    try
                    {
                        IWebElement divFilmText = browserInstance.FindElement(By.XPath(FilmDescriptionXpathLocator));
                        textToOutput += (divFilmText.Text + "\n");
                    }
                    catch (Exception)
                    {
                        textToOutput += "No any data about this film.\n";
                    }
                    browserInstance.Navigate().Back();
                }
            }
            browserInstance.Close();
            // --- output results to console ---
            Console.WriteLine(textToOutput);
            // --- output results to file ---
            using (StreamWriter outputFile = new StreamWriter(@"OutputFile.txt"))
            outputFile.WriteLine(textToOutput);

            //Console.ReadKey();
        }
    }
}
